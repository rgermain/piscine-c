/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_is_prime.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/04 12:33:00 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/04 14:15:50 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

int	ft_is_prime(int nb)
{
	int a;

	a = 3;
	while ((!nb % 2 && nb != 2) || nb <= 1)
		return (0);
	while (a * a < nb)
	{
		if (nb % a == 0)
			return (0);
		a += 2;
	}
	return (1);
}

int	ft_find_next_prime(int nb)
{
	if (nb < 3)
		return (2);
	if (nb % 2 == 0)
		nb++;
	while (!ft_is_prime(nb))
	{
		nb += 2;
	}
	return (nb);
}
