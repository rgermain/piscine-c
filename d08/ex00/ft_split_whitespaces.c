/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_split_whitespaces.c                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/09 19:47:31 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/10 16:25:51 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_test_space(char *str, int a)
{
	if (str[a] == ' ' || str[a] == 9 || str[a] == '\n')
		return (1);
	else
		return (0);
}

int		ft_strlen(char *str, int a)
{
	int b;

	b = 0;
	while (ft_test_space(str, a))
		a++;
	while (!ft_test_space(str, a))
	{
		a++;
		b++;
	}
	return (b);
}

int		ft_strlen_word(char *str, int a)
{
	int	b;

	b = 0;
	while (ft_test_space(str, a))
		a++;
	while (str[a] != '\0')
	{
		a++;
		while (!ft_test_space(str, a))
			a++;
		if (ft_test_space(str, a))
			b++;
		while (ft_test_space(str, a))
			a++;
	}
	return (b);
}

char	**ft_split_whitespaces(char *str)
{
	int		longa;
	int		larg;
	int		a;
	char	**tab;

	longa = 0;
	a = 0;
	while (ft_test_space(str, a))
		a++;
	if (!(tab = malloc(sizeof(char *) * (ft_strlen_word(str, a)))))
		return (NULL);
	while (str[a] != '\0')
	{
		larg = 0;
		tab[longa] = malloc(sizeof(char) * (ft_strlen(str, a) + 1));
		while (!ft_test_space(str, a))
			tab[longa][larg++] = str[a++];
		tab[longa++][larg] = '\0';
		a++;
		while (ft_test_space(str, a))
			a++;
	}
	tab[longa] = 0;
	return (tab);
}
