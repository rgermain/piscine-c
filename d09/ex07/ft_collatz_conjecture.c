/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_collatz_conjecture.c                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/10 11:34:23 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/10 17:06:25 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

unsigned int	ft_collatz_conjecture(unsigned int base)
{
	if (base != 1)
	{
		if ((base % 2) == 0)
			return (1 + ft_collatz_conjecture(base / 2));
		else
			return (2 + ft_collatz_conjecture((3 * base + 1) / 2));
	}
	return (0);
}
