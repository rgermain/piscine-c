/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_destroy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/09 23:43:10 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/10 17:07:10 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ultimator.h"
#include <stdlib.h>

void	ft_destroy(char ***factory)
{
	int a;
	int b;

	a = 0;
	while (factory[a])
	{
		b = 0;
		while (factory[a][b])
			free(factory[a][b++]);
		free(factory[a][b]);
		a++;
	}
}
