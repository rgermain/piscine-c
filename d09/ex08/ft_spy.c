/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_spy.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/10 11:42:30 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/10 12:18:57 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int a;

	a = 0;
	while (str[a] != '\0')
		ft_putchar(str[a++]);
}

void	ft_spy(char *str)
{
	char	*maj;
	char	*min;

