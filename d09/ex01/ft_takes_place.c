/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_takes_place.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/09 22:28:39 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/10 11:51:31 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdio.h>

void	ft_takes_place(int hour)
{
	printf("THE FOLLOWING TAKES PLACE BETWEEN");
	if (hour == 23)
		printf(" 11.00 P.M. AND 12.00 A.M\n");
	if (hour == 24)
		printf(" 12.00 A.M. AND 1.00 A.M\n");
	if (hour == 11)
		printf(" 11.00 A.M. AND 12.00 P.M\n");
	if (hour == 12)
		printf(" 12.00 P.M. AND 1.00 P.M\n");
	if (hour < 12)
		printf(" %d.00 A.M. AND %d.00 A.M\n", hour, hour + 1);
	if (hour > 12)
		printf(" %d.00 P.M. AND %d.00 P.M\n", hour - 12, hour - 11);
}
