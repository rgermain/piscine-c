/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_scrambler.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/10 12:45:13 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/10 12:47:00 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	ft_scrambler(int ***a, int *b, int *******c, int ****d)
{
	int temp;

	temp = ***a;
	***a = *b;
	*b = *******c;
	*******c = ****d;
	****d = temp;
}
