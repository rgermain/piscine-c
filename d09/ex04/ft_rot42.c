/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_rot42.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/09 22:50:42 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/09 23:34:15 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

char	ft_rev_maj(char c)
{
	int		a;
	char	*maj;

	a = c - 65;
	maj = "QRSTUVWXYZABCDEFGHIJKLMNOP";
	return (maj[a]);
}

char	ft_rev_min(char c)
{
	int		a;
	char	*min;

	a = c - 97;
	min = "qrstuvwxyzabcdefghijklmnop";
	return (min[a]);
}

char	*ft_rot42(char *str)
{
	int a;

	a = 0;
	while (str[a] != '\0')
	{
		if (str[a] >= 'A' && str[a] <= 'Z')
		{
			str[a] = ft_rev_maj(str[a]);
			a++;
		}
		else if (str[a] >= 'a' && str[a] <= 'z')
		{
			str[a] = ft_rev_min(str[a]);
			a++;
		}
		else
			a++;
	}
	return (str);
}
