/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_concat_params.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/08 16:24:49 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/09 11:35:51 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_argvlen(int argc, char **argv)
{
	int a;
	int b;
	int c;

	a = 1;
	c = 0;
	while (a < argc)
	{
		b = 0;
		while (argv[a][b] != '\0')
		{
			b++;
			c++;
		}
		a++;
		c++;
	}
	return (c);
}

char	*ft_concat_params(int argc, char **argv)
{
	int		a;
	int		b;
	int		c;
	char	*str;

	a = 1;
	c = 0;
	str = malloc(sizeof(*str) * (ft_argvlen(argc, argv)));
	while (a < argc)
	{
		b = 0;
		while (argv[a][b] != '\0')
		{
			str[c] = argv[a][b];
			b++;
			c++;
		}
		str[c] = '\n';
		a++;
		c++;
	}
	str[c] = '\0';
	return (str);
}
