/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strdup.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/08 13:08:18 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/09 19:49:19 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
	int		len;
	int		a;
	char	*str;

	a = 0;
	len = 0;
	while (src[len] != '\0')
		len++;
	str = (char*)malloc(sizeof(*str) * (len + 1));
	while (a < len)
	{
		str[a] = src[a];
		a++;
	}
	str[a] = '\0';
	return (str);
}
