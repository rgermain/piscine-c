/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_range.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/08 14:59:02 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/09 11:06:14 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int	len;
	int	a;
	int	*str;

	len = 0;
	a = 0;
	if (min >= max)
	{
		*str = 0;
		return (str);
	}
	str = (int*)malloc(sizeof(*str) * (min - max));
	while (min <= max)
	{
		str[a] = min;
		min++;
		a++;
	}
	return (str);
}
