/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_range.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/08 14:59:02 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/09 11:07:24 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int	len;
	int	a;
	int	*str;

	len = 0;
	a = 0;
	if (min >= max)
	{
		*range = 0;
		return (a);
	}
	str = (int*)malloc(sizeof(*str) * (min - max));
	while (min <= max)
	{
		str[a] = min;
		min++;
		a++;
	}
	*range = str;
	return (a);
}
