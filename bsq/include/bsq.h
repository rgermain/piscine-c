/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bsq.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rsereir <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/19 04:15:43 by rsereir      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 23:38:07 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H

# include <stdlib.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <fcntl.h>
# include <unistd.h>

/*
** VARIABLES GLOBALES
*/
# define READ_LEN 500000
# define BUFF_READ_INPUT 1024

/*
** STRUCTURES
*/
typedef struct s_map	t_map;

struct	s_map
{
	char		**map;
	int			tray_size;
	int			lines;
	int			tray_lines;
	int			columns;
	int			size;
	int			start_x;
	int			start_y;
	int			x;
	int			y;
	char		empty;
	char		block;
	char		result;
};

/*
** PROTOTYPAGE
*/
void	ft_putstr(char *str);
int		ft_strlen(char *str);
int		ft_atoi(char *str);
int		ft_str_is_numeric(char *str);
int		ft_ilen(int nb);

void	free_map(t_map *map);
void	free_tray(t_map *map);
void	resolve_tray(t_map **map);
int		count_tray_size(char *path);
int		count_map_lines(char **map);
int		check_square(t_map *map, int size, int x, int y);
int		find_square(t_map **map, int size, int find);
void	display_map(t_map *map);
int		is_valid_char(t_map *map, char c);
void	is_error(void);
int		get_size_lines(t_map **map, char *tray, int size, int i);
char	*get_config(char **tray);
char	*remove_config_line(char *init_tray, int len);
char	**get_map(char *tray, t_map *m, int i, int y);
int		init_map(t_map **map, char *tray);
int		get_tray_input(void);
int		get_tray_path(char *path, int count);
int		bsq(char *tray, int tray_size);
int		main(int ac, char **argv);

#endif
