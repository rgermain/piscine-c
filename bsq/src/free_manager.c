/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   free_manager.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rsereir <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 21:49:38 by rsereir      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/21 18:03:45 by rsereir     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "bsq.h"

void	free_map(t_map *map)
{
	int		y;

	y = 0;
	while (map->map[y])
	{
		free(map->map[y]);
		y++;
	}
	free(map->map);
}

void	free_tray(t_map *map)
{
	free_map(map);
	free(map);
}
