/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   map_manager.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rsereir <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 14:31:03 by rsereir      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 21:59:46 by rsereir     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "bsq.h"

int		count_map_lines(char **map)
{
	int		y;

	y = 0;
	while (map[y])
		y++;
	return (y);
}

int		get_size_lines(t_map **map, char *tray, int size, int i)
{
	int		len;
	int		nb_lines;
	t_map	*m;

	m = *map;
	while (tray[i++] != '\n')
		size = i;
	nb_lines = 1;
	len = 0;
	while (tray[i] != '\0')
	{
		len++;
		if (tray[i] == '\n')
		{
			if (len - 1 != size)
				return (0);
			len = 0;
			nb_lines++;
		}
		i++;
	}
	m->tray_lines = nb_lines;
	return (size);
}

char	**get_map(char *tray, t_map *m, int i, int y)
{
	char	**map;
	int		x;

	m->tray_size = ((m->tray_size - 4 - ft_ilen(m->lines)) / m->lines);
	if (!(map = malloc(sizeof(char*) * (m->lines + 1))))
		return (NULL);
	while (tray[i] != '\0')
	{
		x = 0;
		if (!(map[y] = malloc(sizeof(char) * m->tray_size + 1)))
			return (NULL);
		while (x < m->tray_size)
		{
			if (!is_valid_char(m, tray[x + i]))
				return (NULL);
			map[y][x] = tray[x + i];
			x++;
		}
		map[y++][x] = '\0';
		i += m->tray_size;
	}
	map[y] = NULL;
	return (map);
}
