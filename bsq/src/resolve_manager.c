/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   resolve_manager.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rsereir <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 14:31:27 by rsereir      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 23:37:29 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "bsq.h"

int		check_square(t_map *m, int size, int x, int y)
{
	int		ligne;
	int		col;
	int		isn_obs;

	ligne = y;
	col = x;
	isn_obs = 1;
	while (ligne < y + size && isn_obs)
	{
		while (col < x + size && isn_obs)
		{
			if (m->map[ligne][col] == m->block)
			{
				m->start_x = col;
				isn_obs = 0;
			}
			col++;
		}
		col = x;
		ligne++;
	}
	if (m->start_y > ligne)
		m->start_y = ligne;
	return (isn_obs);
}

int		find_square(t_map **map, int size, int find)
{
	int		y;
	int		x;
	int		temp_x;
	t_map	*m;
	int		temp_y;

	m = *map;
	y = m->start_y;
	x = m->start_x;
	while (y + size <= m->lines && !find)
	{
		while (x + size <= m->columns && !find)
		{
			find = check_square(m, size, x, y);
			temp_x = x;
			x = m->start_x;
			m->start_x = temp_x;
			x++;
		}
		x = 0;
		temp_y = y;
		y = m->start_y + 1;
		m->start_y = temp_y;
	}
	return (find == 1) ? 1 : 0;
}

void	resolve_tray(t_map **map)
{
	int		temp;
	int		size;
	t_map	*m;

	m = *map;
	temp = 2;
	size = 1;
	while ((temp == 2 || temp == 1) && size <= m->lines && size <= m->columns)
	{
		temp = find_square(map, size, 0);
		if (temp != 0)
		{
			m->x = m->start_x;
			m->y = m->start_y;
			size++;
		}
		if (temp == 0 && size == 1)
			size--;
	}
	m->size = size - 1;
}

int		bsq(char *tray, int tray_size)
{
	t_map	*map;

	if (!(map = malloc(sizeof(t_map))))
		return (0);
	map->tray_size = tray_size;
	if (!init_map(&map, tray))
		return (0);
	resolve_tray(&map);
	display_map(map);
	free_tray(map);
	free(tray);
	return (1);
}
