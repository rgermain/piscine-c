/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   error_manager.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rsereir <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 11:36:56 by rsereir      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/21 00:06:01 by rsereir     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "bsq.h"

int		is_valid_char(t_map *map, char c)
{
	if (c == map->empty || c == map->block || c == '\n')
		return (1);
	return (0);
}

void	is_error(void)
{
	ft_putstr("map error\n");
}
