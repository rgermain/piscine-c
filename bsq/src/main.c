/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rsereir <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 11:38:36 by rsereir      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 22:16:44 by rsereir     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "bsq.h"

int		main(int ac, char **argv)
{
	int i;

	i = 1;
	if (ac == 1)
	{
		if (!get_tray_input())
			is_error();
	}
	else
	{
		while (i < ac)
		{
			if (!get_tray_path(argv[i++], -1))
				is_error();
			if (i < ac)
				ft_putstr("\n");
		}
	}
	return (0);
}
