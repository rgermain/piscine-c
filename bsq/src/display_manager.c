/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   display_manager.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rsereir <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 11:42:43 by rsereir      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/21 17:04:22 by rsereir     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "bsq.h"

char	**replace_result(t_map *map)
{
	int x;
	int y;
	int size;

	y = map->y;
	size = map->size;
	while (y < map->y + size)
	{
		x = map->x;
		while (x < map->x + size)
		{
			map->map[y][x] = map->result;
			x++;
		}
		y++;
	}
	return (map->map);
}

void	display_map(t_map *map)
{
	int		y;

	y = 0;
	if (map->size > 0)
		map->map = replace_result(map);
	while (map->map[y])
		write(1, map->map[y++], map->columns + 1);
}
