/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   read_manager.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rsereir <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 15:10:55 by rsereir      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 18:06:50 by rsereir     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "bsq.h"

int		count_tray_size(char *path)
{
	char	buffer[READ_LEN];
	int		fd;
	int		chars_read;
	int		count;

	count = 0;
	if ((fd = open(path, O_RDONLY)) == -1)
		return (0);
	while ((chars_read = read(fd, buffer, READ_LEN)) > 0)
		count += chars_read;
	close(fd);
	return (count);
}

int		get_tray_input(void)
{
	int			fd;
	char		*buff;
	long long	buff_size;
	int			ret;
	int			chars_read;

	chars_read = 0;
	buff_size = BUFF_READ_INPUT;
	if (!(buff = malloc(sizeof(char) * buff_size)))
		return (0);
	fd = open("map", O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
	while ((ret = read(0, buff, buff_size)) > 0)
	{
		write(fd, buff, ret);
		buff_size += ret;
		chars_read += ret;
		if (!(buff = malloc(sizeof(char) * buff_size)))
			return (0);
	}
	close(fd);
	free(buff);
	return (get_tray_path("map", chars_read));
}

int		get_tray_path(char *path, int count)
{
	char	*tray;
	int		fd;
	int		i;

	if (count == -1)
		count = count_tray_size(path);
	if ((fd = open(path, O_RDONLY)) == -1)
		return (0);
	if (!(tray = malloc(sizeof(char) * count)))
		return (0);
	i = read(fd, tray, count);
	tray[i] = '\0';
	close(fd);
	return (bsq(tray, count));
}
