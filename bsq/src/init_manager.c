/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   init_manager.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rsereir <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/20 11:37:17 by rsereir      #+#   ##    ##    #+#       */
/*   Updated: 2018/08/22 22:00:11 by rsereir     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "bsq.h"

char	*get_config(char **tray)
{
	char	*init_tray;
	char	*config;
	int		i;
	int		len;

	i = 0;
	len = 0;
	init_tray = *tray;
	while (init_tray[len] != '\n' && init_tray[len] != '\0')
		len++;
	if (!(config = malloc(sizeof(char) * (len + 1))) || len < 4)
		return (NULL);
	while (init_tray[i] != '\n')
	{
		config[i] = init_tray[i];
		i++;
	}
	config[i] = '\0';
	*tray = remove_config_line(init_tray, len);
	return (config);
}

char	*remove_config_line(char *init_tray, int len)
{
	char	*real_tray;
	int		i;

	i = 0;
	if (!(real_tray = malloc(sizeof(char) * (ft_strlen(init_tray) - len + 1))))
		return (NULL);
	while (init_tray[i + len + 1] != '\0')
	{
		real_tray[i] = init_tray[i + len + 1];
		i++;
	}
	real_tray[i] = '\0';
	return (real_tray);
}

int		init_map(t_map **map, char *tray)
{
	t_map	*init_map;
	char	*config;

	if (!(config = get_config(&tray)))
		return (0);
	init_map = *map;
	init_map->size = 0;
	init_map->start_x = 0;
	init_map->start_y = 0;
	init_map->x = 0;
	init_map->y = 0;
	init_map->empty = config[ft_strlen(config) - 3];
	init_map->block = config[ft_strlen(config) - 2];
	init_map->result = config[ft_strlen(config) - 1];
	config[ft_strlen(config) - 3] = '\0';
	if (!ft_str_is_numeric(config))
		return (0);
	init_map->lines = ft_atoi(config);
	free(config);
	init_map->columns = get_size_lines(map, tray, 0, 0);
	if (init_map->lines != init_map->tray_lines)
		return (0);
	if (!(init_map->map = get_map(tray, init_map, 0, 0)))
		return (0);
	return (init_map->lines < 1 || init_map->columns < 1) ? 0 : 1;
}
