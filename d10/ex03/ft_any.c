/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_any.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 16:01:11 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/14 12:03:50 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

int	ft_any(char **tab, int (*f)(char*))
{
	int a;

	a = 0;
	while (tab[a])
	{
		if (f(tab[a]) == 1)
			return (1);
		a++;
	}
	return (0);
}
