/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_h.h                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 18:45:56 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/13 20:20:40 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_H_H
# define FT_H_H

void	ft_putchar(char c);
void	ft_calcul(int params1, char opera, int params2);
int		ft_atoi(char *str);
void	ft_putnbr(int nb);
void	ft_putchar(char c);
void	ft_putstr(char *str);
void	addition(int params1, int params2);
void	substraction(int params1, int params2);
void	multiplication(int params1, int params2);
void	division(int params1, int params2);
void	modulo(int params1, int params2);

#endif
