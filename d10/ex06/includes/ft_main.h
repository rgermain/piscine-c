/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_main.h                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 18:53:46 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/13 18:58:19 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_MAIN_H
# define FT_MAIN_H

int	main(int argc, char **argv);

#endif
