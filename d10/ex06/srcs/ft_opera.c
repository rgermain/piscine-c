/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_opera.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 19:24:57 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/14 11:28:03 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_h.h"

void	substraction(int params1, int params2)
{
	ft_putnbr(params1 - params2);
}

void	division(int params1, int params2)
{
	if (params2 == 0)
		ft_putstr("Stop : division by zero");
	else
		ft_putnbr(params1 / params2);
}

void	multiplication(int params1, int params2)
{
	ft_putnbr(params1 * params2);
}

void	addition(int params1, int params2)
{
	ft_putnbr(params1 + params2);
}

void	modulo(int params1, int params2)
{
	if (params2 == 0)
		ft_putstr("Stop : modulo by zero");
	else
		ft_putnbr(params1 % params2);
}
