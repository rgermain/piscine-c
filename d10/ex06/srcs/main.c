/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 17:29:39 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/13 20:46:33 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_h.h"
#include "ft_main.h"

int		main(int argc, char **argv)
{
	int		params1;
	int		params2;

	params1 = 0;
	params2 = 0;
	if (argc == 4)
	{
		params1 = ft_atoi(argv[1]);
		params2 = ft_atoi(argv[3]);
		ft_calcul(params1, argv[2][0], params2);
	}
	return (0);
}
