/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_calcul.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 20:50:15 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/14 11:28:22 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_h.h"

void	ft_calcul(int params1, char opera, int params2)
{
	void	(*f[5])(int, int);

	f[0] = substraction;
	f[1] = division;
	f[2] = multiplication;
	f[3] = addition;
	f[4] = modulo;
	if (opera == '-')
		f[0](params1, params2);
	else if (opera == '/')
		f[1](params1, params2);
	else if (opera == '*')
		f[2](params1, params2);
	else if (opera == '+')
		f[3](params1, params2);
	else if (opera == '%')
		f[4](params1, params2);
	else
		ft_putchar('0');
	ft_putchar('\n');
}
