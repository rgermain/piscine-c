/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_map.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 16:05:58 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/14 16:25:51 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_map(int *tab, int length, int (*f)(int))
{
	int *str;

	if (!(str = malloc(sizeof(int) * (length))))
		return (NULL);
	while (tab[a])
	{
		*str++ = f(tab[a]);
		a++;
	}
	return (str);
}
