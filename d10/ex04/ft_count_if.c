/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_count_if.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 16:08:44 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/14 12:00:57 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

int	ft_count_if(char **tab, int (*f)(char*))
{
	int count;
	int a;
	int b;

	count = 0;
	a = 0;
	while (tab[a])
	{
		count = count + f(tab[a]);
		a++;
	}
	return (count);
}
