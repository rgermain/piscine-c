/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strlcpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/06 13:15:10 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/07 09:21:50 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

unsigned	int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	unsigned int a;

	a = 0;
	while (src[a])
	{
		if (a < (size - 1))
			dest[a] = src[a];
		a++;
	}
	dest[a] = '\0';
	return (a);
}
