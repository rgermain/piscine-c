/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_sort_params.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/07 12:34:33 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/07 13:58:25 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstr(char *str)
{
	int a;

	a = 0;
	while (str[a] != '\0')
		ft_putchar(str[a++]);
}

int		ft_strcmp(char *s1, char *s2)
{
	int	a;
	int	b;

	a = 0;
	b = 0;
	while (1)
	{
		b += (s1[a] - s2[a]);
		if (s1[a] == '\0' && s2[a] == '\0')
			return (b);
		if (s1[a] == s2[a])
			a++;
		else
			return (b);
	}
}

void	ft_sort_params(int argc, char **argv)
{
	int		a;
	int		b;
	char	*c;

	a = 0;
	b = 0;
	while (b != 1)
	{
		a = 0;
		b = 1;
		while (a < (argc - 1))
		{
			if (ft_strcmp(argv[a], argv[a + 1]) >= 1)
			{
				c = argv[a];
				argv[a] = argv[a + 1];
				argv[a + 1] = c;
				b = 0;
			}
			a++;
		}
	}
}

int		main(int argc, char **argv)
{
	int	a;

	a = 1;
	ft_sort_params(argc - 1, argv + 1);
	while (a < argc)
	{
		ft_putstr(argv[a++]);
		ft_putchar('\n');
	}
	return (0);
}
