/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_rev_params.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/07 13:55:59 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/07 13:56:02 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstr(char *str)
{
	int a;

	a = 0;
	while (str[a] != '\0')
		ft_putchar(str[a++]);
}

int		main(int argc, char **argv)
{
	int a;

	a = argc - 1;
	while (a >= 1)
	{
		ft_putstr(argv[a--]);
		ft_putchar('\n');
	}
	return (0);
}
