/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: themarch <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/04 00:47:43 by themarch     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/05 19:44:24 by themarch    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	rush(int x, int y);

int		ft_best_atoi(char *argv)
{
	int		x;
	int		nb;

	x = 0;
	nb = 0;
	while (argv[x])
	{
		nb = nb * 10 + argv[x] - '0';
		x++;
	}
	return (nb);
}

int		main(int argc, char **argv)
{
	int		x;
	int		y;
	int		i;
	int		j;

	j = 1;
	if (argc != 3)
		return (0);
	while (j <= 2)
	{
		i = 0;
		while (argv[j][i])
		{
			if (argv[j][i] >= '0' && argv[j][i] <= '9')
				i++;
			else
				return (0);
		}
		j++;
	}
	x = ft_best_atoi(argv[1]);
	y = ft_best_atoi(argv[2]);
	rush(x, y);
	return (0);
}
