/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/15 11:34:51 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/19 14:31:12 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_h.h"

char	*ft_tab_mal(void)
{
	int		a;
	int		text;
	char	tab_temp[1];
	char	*tab;

	a = 0;
	tab = malloc(sizeof(char*));
	while ((text = read(0, tab_temp, 1)) > 0)
	{
		tab[a] = tab_temp[0];
		a++;
	}
	return (tab);
}

int		main(void)
{
	char	*tab;
	int		len_larg;
	int		len_long;
	int		a;

	a = 0;
	tab = ft_tab_mal();
	len_long = ft_strlen_long(tab);
	len_larg = ft_strlen_larg(tab, len_long);
	if (len_long > 0 && len_larg > 0)
		a = ft_check_colle(tab, len_long, len_larg);
	if (a == 0)
		ft_putstr("aucune");
	ft_putchar('\n');
	return (0);
}
