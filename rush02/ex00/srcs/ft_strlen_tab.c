/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strlen_tab.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/19 13:32:31 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/19 13:34:08 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_h.h"

int		ft_strlen_larg(char *str, int len_long)
{
	int a;
	int b;
	int c;

	a = 0;
	b = 0;
	c = len_long;
	while (str[a] != '\0')
	{
		if (str[a] == '\n')
		{
			b++;
			a++;
		}
		else
		{
			a++;
		}
	}
	return (b);
}

int		ft_strlen_long(char *str)
{
	int a;

	a = 0;
	while (str[a] != '\0' && str[a] != '\n')
		a++;
	return (a);
}
