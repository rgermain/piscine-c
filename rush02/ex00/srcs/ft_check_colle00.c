/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_check_colle00.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: ftilly <marvin@le-101.fr>                  +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/18 14:02:46 by ftilly       #+#   ##    ##    #+#       */
/*   Updated: 2018/08/19 12:07:46 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_h.h"

int		ft_check00(int x, int y, int a, int b)
{
	if ((a == 0 && b == 0) || (a == x - 1 && b == 0) ||
		(a == x - 1 && b == y - 1) || (a == 0 && b == y - 1))
		return (1);
	else if ((a == 0 && b != 0 && b != y - 1) ||
		(a == x - 1 && b != 0 && b != y - 1))
		return (2);
	else if ((b == 0 && a != 0 && a != x - 1) ||
		(a != x - 1 && a != 0 && b == y - 1))
		return (3);
	return (0);
}

int		ft_check_colle00(char *tab, int x, int y, int c)
{
	int a;
	int b;

	b = 0;
	a = 0;
	while (b < y && x > 0 && y > 0 && tab[c] != '\0')
	{
		if (tab[c] == '\n')
		{
			b++;
			a = -1;
		}
		else if ((ft_check00(x, y, a, b) == 1) && tab[c] != 'o')
			return (0);
		else if ((ft_check00(x, y, a, b) == 2) && tab[c] != '|')
			return (0);
		else if ((ft_check00(x, y, a, b) == 3) && tab[c] != '-')
			return (0);
		else if ((ft_check00(x, y, a, b) == 0) && tab[c] != ' ')
			return (0);
		c++;
		a++;
	}
	return (1);
}
