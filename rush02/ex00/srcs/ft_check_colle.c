/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_check_colle.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: ftilly <marvin@le-101.fr>                  +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/18 14:23:58 by ftilly       #+#   ##    ##    #+#       */
/*   Updated: 2018/08/19 12:26:14 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_h.h"

int		ft_print_name(char *str, int len_long, int len_larg, int index)
{
	if (index > 0)
		ft_putstr(" || ");
	ft_putchar('[');
	ft_putstr(str);
	ft_putstr("] [");
	ft_putnbr(len_long);
	ft_putstr("] [");
	ft_putnbr(len_larg);
	ft_putchar(']');
	return (1);
}

int		ft_check_colle(char *tab, int len_long, int len_larg)
{
	int c;
	int index;

	c = 0;
	index = 0;
	if ((ft_check_colle00(tab, len_long, len_larg, c)) == 1)
		index = ft_print_name("colle-00", len_long, len_larg, index);
	if ((ft_check_colle01(tab, len_long, len_larg, c)) == 1)
		index = ft_print_name("colle-01", len_long, len_larg, index);
	if ((ft_check_colle02(tab, len_long, len_larg, c)) == 1)
		index = ft_print_name("colle-02", len_long, len_larg, index);
	if ((ft_check_colle03(tab, len_long, len_larg, c)) == 1)
		index = ft_print_name("colle-03", len_long, len_larg, index);
	if ((ft_check_colle04(tab, len_long, len_larg, c)) == 1)
		index = ft_print_name("colle-04", len_long, len_larg, index);
	else
		return (index);
	return (index);
}
