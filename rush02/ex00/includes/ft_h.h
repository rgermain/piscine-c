/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_h.h                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 18:45:56 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/19 13:17:45 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_H_H
# define FT_H_H
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>

void	ft_putchar(char c);
void	ft_putnbr(int nb);
void	ft_putstr(char *str);
int		main(void);
int		ft_check_colle(char *tab, int len_long, int len_larg);
int		ft_check_colle00(char *tab, int x, int y, int c);
int		ft_check_colle01(char *tab, int x, int y, int c);
int		ft_check_colle02(char *tab, int x, int y, int c);
int		ft_check_colle03(char *tab, int x, int y, int c);
int		ft_check_colle04(char *tab, int x, int y, int c);
char	*ft_tab_mal(void);
int		ft_strlen_long(char *str);
int		ft_strlen_larg(char *str, int len_long);
int		ft_check00(int x, int y, int a, int b);
int		ft_check01(int x, int y, int a, int b);
int		ft_check02(int x, int y, int a, int b);
int		ft_check03(int x, int y, int a, int b);
int		ft_check04(int x, int y, int a, int b);
int		ft_print_name(char *str, int len_long, int len_larg, int index);

#endif
