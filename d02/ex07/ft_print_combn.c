/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_print_combn.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/01 15:31:51 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/02 10:49:33 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print_combn3(int *tab, int n)
{
	int i;
	int j;

	i = 1;
	j = 1;
	while (i < n)
	{
		if (tab[i - 1] >= tab[i])
			j = 0;
		i++;
	}
	if (j)
	{
		i = 0;
		while (i < n)
			ft_putchar(tab[i++] + '0');
		if (tab[0] < (10 - n))
		{
			ft_putchar(',');
			ft_putchar(' ');
		}
	}
}

void	ft_print_combn2(int *tab, int n, int i)
{
	ft_print_combn3(tab, n);
	tab[n - 1]++;
	i = n;
	while (i && n > 1)
	{
		i--;
		if (tab[i] > 9)
		{
			tab[i - 1]++;
			tab[i] = tab[i - 1];
		}
	}
}

void	ft_print_combn(int n)
{
	int i;
	int tab[n];

	i = 0;
	if (n > 9 || n < 1)
		return ;
	while (i < n)
	{
		tab[i] = i + 1;
		i++;
	}
	while (tab[0] <= (10 - n) && n < 10 && n > 0)
		ft_print_combn2(tab, n, i);
	ft_putchar('\n');
}
