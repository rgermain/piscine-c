/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_print_comb.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/01 14:25:07 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/02 10:52:57 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print_comb2(int a[3])
{
	ft_putchar(a[0] + '0');
	ft_putchar(a[1] + '0');
	ft_putchar(a[2] + '0');
	if (a[0] != 7)
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void	ft_print_comb(void)
{
	int a[3];

	a[0] = 0;
	a[1] = 1;
	a[2] = 2;
	while (a[0] <= 7)
	{
		a[1] = a[0] + 1;
		while (a[1] <= 8)
		{
			a[2] = a[1] + 1;
			while (a[2] <= 9)
			{
				ft_print_comb2(a);
				a[2]++;
			}
			a[1]++;
		}
		a[0]++;
	}
}
