/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_print_comb2.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/01 14:57:43 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/08/02 10:46:50 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print_comb3(int a, int b)
{
	ft_putchar(a / 10 + '0');
	ft_putchar(a % 10 + '0');
	ft_putchar(' ');
	ft_putchar(b / 10 + '0');
	ft_putchar(b % 10 + '0');
	if (a != 98)
		ft_putchar(',');
	else
		ft_putchar('\n');
}

void	ft_print_comb2(void)
{
	int a;
	int b;

	a = 00;
	b = 01;
	while (a != 99)
	{
		b = a + 1;
		while (b <= 99)
		{
			ft_print_comb3(a, b);
			b++;
		}
		a++;
	}
}
